package com.noir.myapplication;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class BaseActivity extends AppCompatActivity implements TaskCompleted {

    public static final int CIRCLE = 2;
    public static final int SQUARE = 3;
    public static final int TRIANGLE = 4;
    protected int currentCheckedButton;
    protected AppPreferences appPreferences;
    private Map<String, String> map = new HashMap<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appPreferences = new AppPreferences(this);

        map = appPreferences.getMap();
        if (map.size() > 0) {
            Map<String, String> updateMap = new HashMap<>(map);
            map.clear();

            for (Map.Entry<String, String> entry : updateMap.entrySet()) {
                sendPack(entry.getValue());
            }
        }
    }

    @Override
    protected void onStop() {
        appPreferences.saveFigure(currentCheckedButton);
        appPreferences.saveQueue(map);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    protected void sendPack(String pack) {
        (new SendPack(this)).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, pack);
    }

    @Override
    public void onPostExecute(Integer result) {

    }

    private class SendPack extends AsyncTask<String, Integer, Integer> {

        private TaskCompleted taskCompleted;
        private Long timeStamp;

        public SendPack(TaskCompleted taskCompleted) {
            this.taskCompleted = taskCompleted;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            timeStamp = Calendar.getInstance().getTime().getTime();
            map.put(String.valueOf(timeStamp), new String());
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            taskCompleted.onPostExecute(integer);
            if (integer == 200) {
                map.remove(String.valueOf(timeStamp));
            }
        }

        @Override
        protected Integer doInBackground(String... params) {
            return 200;
        }
    }
}
