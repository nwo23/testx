package com.noir.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class NextActivity extends BaseActivity {

    private Button ibSquare, ibCircle, ibTriangle;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_next);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ibCircle = findViewById(R.id.circle);
        ibSquare = findViewById(R.id.square);
        ibTriangle = findViewById(R.id.triangle);
        textView = findViewById(R.id.text);

        Bundle arguments = getIntent().getExtras();
        activateButton(arguments.getInt("figure"));
        textView.setText(arguments.getString("time"));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("figure", currentCheckedButton);
        setResult(Activity.RESULT_OK, intent);
        super.onBackPressed();
    }

    public void clickTriangle(View view) {
        activateButton(TRIANGLE);
        sendPack(new String());
    }

    public void clickSquare(View view) {
        activateButton(SQUARE);
        sendPack(new String());
    }

    public void clickCircle(View view) {
        activateButton(CIRCLE);
        sendPack(new String());
    }

    private void activateButton(Integer figure) {
        ibTriangle.setBackgroundColor(Color.TRANSPARENT);
        ibSquare.setBackgroundColor(Color.TRANSPARENT);
        ibCircle.setBackgroundColor(Color.TRANSPARENT);

        switch (figure) {
            case CIRCLE:
                ibCircle.setBackgroundColor(Color.GRAY);
                currentCheckedButton = CIRCLE;
                break;
            case SQUARE:
                ibSquare.setBackgroundColor(Color.GRAY);
                currentCheckedButton = SQUARE;
                break;
            case TRIANGLE:
                ibTriangle.setBackgroundColor(Color.GRAY);
                currentCheckedButton = TRIANGLE;
                break;
        }
    }
}
