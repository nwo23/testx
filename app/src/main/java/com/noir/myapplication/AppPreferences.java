package com.noir.myapplication;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class AppPreferences {

    private final String APP_SHARED_PREFS = AppPreferences.class.getSimpleName();
    private final String KEY = "figure";
    private final String KEY_MAP = "map";
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor prefsEditor;

    public AppPreferences(Context context) {
        this.sharedPreferences = context.getSharedPreferences(APP_SHARED_PREFS, Activity.MODE_PRIVATE);
        this.prefsEditor = sharedPreferences.edit();
    }

    public Integer getFigure() {
        return sharedPreferences.getInt(KEY, BaseActivity.TRIANGLE);
    }

    public void saveFigure(Integer figure) {
        prefsEditor.putInt(KEY, figure);
        prefsEditor.commit();
    }

    public void saveQueue(Map<String, String> map) {
        JSONObject json = new JSONObject(map);
        prefsEditor.remove(KEY_MAP);
        prefsEditor.putString(KEY_MAP, json.toString());
        prefsEditor.commit();
    }

    public Map<String, String> getMap() {
        JSONObject jsonObject;
        Map<String, String> map = new HashMap<>();
        try {
            jsonObject = new JSONObject(sharedPreferences.getString(KEY_MAP, null));
            Iterator<String> keys = jsonObject.keys();
            while (keys.hasNext()) {
                String key = keys.next();
                String value = jsonObject.getString(key);
                map.put(key, value);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return map;
    }
}
