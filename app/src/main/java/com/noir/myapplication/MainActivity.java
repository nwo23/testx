package com.noir.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class MainActivity extends BaseActivity {

    private ImageButton ibSquare, ibCircle, ibTriangle;
    public static final int REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ibCircle = findViewById(R.id.circle);
        ibSquare = findViewById(R.id.square);
        ibTriangle = findViewById(R.id.triangle);

        activateButton(appPreferences.getFigure());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_CODE || resultCode == Activity.RESULT_OK) {
            activateButton(data.getExtras().getInt("figure"));
        }
    }

    public void nextActivity(View view) {
        Intent intent = new Intent(this, NextActivity.class);
        intent.putExtra("figure", currentCheckedButton);
        intent.putExtra("time", (new SimpleDateFormat("HH:mm")).format(Calendar.getInstance().getTime()));
        startActivityForResult(intent, REQUEST_CODE);
    }

    public void clickTriangle(View view) {
        activateButton(TRIANGLE);
        sendPack(new String());
    }

    public void clickSquare(View view) {
        activateButton(SQUARE);
        sendPack(new String());
    }

    public void clickCircle(View view) {
        activateButton(CIRCLE);
        sendPack(new String());
    }

    private void activateButton(Integer figure) {
        ibTriangle.setBackgroundColor(Color.TRANSPARENT);
        ibSquare.setBackgroundColor(Color.TRANSPARENT);
        ibCircle.setBackgroundColor(Color.TRANSPARENT);

        switch (figure) {
            case CIRCLE:
                ibCircle.setBackgroundColor(Color.GRAY);
                currentCheckedButton = CIRCLE;
                break;
            case SQUARE:
                ibSquare.setBackgroundColor(Color.GRAY);
                currentCheckedButton = SQUARE;
                break;
            case TRIANGLE:
                ibTriangle.setBackgroundColor(Color.GRAY);
                currentCheckedButton = TRIANGLE;
                break;
        }
    }
}
