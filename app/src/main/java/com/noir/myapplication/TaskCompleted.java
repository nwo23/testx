package com.noir.myapplication;

public interface TaskCompleted {
    void onPostExecute(Integer result);
}
